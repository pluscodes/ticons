# Ticons

## Requirements

## Installation

Ticons is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Ticons', :git => 'https://pluscodes@bitbucket.org/pluscodes/ticons.git'
```

## Author

Thies Hagedorn, thies@pluscodes.de

## License

Ticons is available under the MIT license. See the LICENSE file for more info.

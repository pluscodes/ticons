//
//  TIWarnRadioActive.swift
//  Pods-TIcons_Example
//
//  Created by Mobile Trend GmbH on 11.03.19.
//

import UIKit

@IBDesignable
public class TIWarnRadioActive: UIView {
    
    @IBInspectable public var animated: Bool = true
    @IBInspectable var animationSpeed: CGFloat = 1.0
    
    var angle: CGFloat = 0.0
    var animationTimer: Timer? = nil
    
    override init (frame: CGRect) {
        super.init(frame: frame)
        initCommon()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initCommon()
    }
    
    func initCommon() {
        if self.animated {
            self.animate(running: true)
        }
    }
    
    
    
    override public func draw(_ rect: CGRect) {

        Ticons.drawWarnRadioActive(
            frame: self.bounds,
            resizing: .aspectFit,
            angle: angle
        )
        
    }
    
    public func animate(running: Bool = true) {

        if running == true {
            self.animationTimer = Timer(
                timeInterval: 0.01,
                target: self,
                selector: #selector(self.animate),
                userInfo: nil,
                repeats: true
            )
            
            if let theTimer = self.animationTimer {
                RunLoop.main.add(theTimer, forMode: RunLoop.Mode.common)
            }
        } else {
            self.animationTimer?.invalidate()
        }
        
    }
    
    @objc func animate() {
        
        self.angle -= self.animationSpeed
        self.setNeedsDisplay()
        
    }
    
}
